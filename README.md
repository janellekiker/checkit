# Checkit

* Gabby Burgard
* Janelle Kiker
* Judith Itkoe
* Mo Kanawi
* Liam Scully

Check It – giving you the opportunity to check a claim, discover the truth and save it if you want.

## Design
* [API design](docs/apis.md)
* [Wireframes](docs/wireframes.md)
* [Integrations](docs/integrations.md)

## Intended market
We are targeting general users to check claims validity that surround in the internet that are accessible. Users will be able to search a claim and will retrieve a list of claims found. Each claim will have the information about the claim, date, claimant, statement’s validity, reviewer, and website url.

## Functionality
* Visitors to the site can search by writing a word, or name that they are interested in then that will filter through to match and retrieve claims to the user.
* Users can click on the claims that got listed on the search page and can go to the actual link where claim was made.
* Claims can be added to a category previously made, or add to a new category.
* Claims can be deleted out of a category.

## Project Initialization
To fully enjoy this application on your local machine, please make sure to follow these steps:
1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run ```docker volume create checkit-data```
4. Run ```docker compose build```
5. Run ```docker compose up```
6. Enjoy Checkit!
