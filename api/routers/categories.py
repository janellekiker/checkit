from fastapi import APIRouter, Depends, Response, HTTPException
from typing import List, Union
from models import (
    CategoryIn,
    CategoryOut,
    Error,
    SuccessMessage,
    UpdateCategory,
)
from queries.categories import CategoriesRepository
from authenticator import authenticator

router = APIRouter()


@router.post("/api/categories", response_model=Union[CategoryOut, Error])
def create(
    category: CategoryIn,
    response: Response,
    repo: CategoriesRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    new_category = repo.create(category, user_id=account_data["id"])
    if isinstance(new_category, Error):
        response.status_code = 400
    return new_category


@router.get("/api/categories", response_model=Union[List[CategoryOut], Error])
def get_all(
    repo: CategoriesRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response = repo.get_all(user_id=account_data["id"])
    if len(response) == 0:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return response


@router.put("/api/categories/{category_id}", response_model=SuccessMessage)
def update_category(
    category_id: int,
    name: UpdateCategory,
    repo: CategoriesRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(category_id, name, user_id=account_data["id"])


@router.delete(
    "/api/categories/{category_name}", response_model=SuccessMessage
)
def delete_category(
    category_name: str,
    repo: CategoriesRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response = repo.delete(category_name, user_id=account_data["id"])
    if response == {"success": False}:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return {"success": True}
