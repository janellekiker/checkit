from fastapi import APIRouter, Depends
from queries.claims import ThirdPartyClaimRespository

router = APIRouter()


@router.get("/api/claims")
def get_third_party_claims(
    query: str,
    repo: ThirdPartyClaimRespository = Depends(),
):
    return {"claim": repo.get_all_claims(query)}
