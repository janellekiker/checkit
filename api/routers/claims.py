from fastapi import APIRouter, Depends, Response, HTTPException
from typing import List, Union
from models import Error, ClaimOut, ClaimIn, SuccessMessage, UpdateClaim
from queries.claims import ClaimsRepository
from authenticator import authenticator


router = APIRouter()


@router.post("/api/saves", response_model=Union[ClaimOut, Error])
def create(
    claim: ClaimIn,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: ClaimsRepository = Depends(),
):
    new_claim = repo.create(claim, user_id=account_data["id"])
    if isinstance(new_claim, Error):
        response.status_code = 400
    return new_claim


@router.get("/api/saves", response_model=Union[List[ClaimOut], Error])
def get_all(
    repo: ClaimsRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(user_id=account_data["id"])


@router.put("/api/saves/{save_id}", response_model=SuccessMessage)
def update_save(
    save_id: int,
    category_name: UpdateClaim,
    repo: ClaimsRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response = repo.update(save_id, category_name, user_id=account_data["id"])
    if response == {"success": False}:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return {"success": True}


@router.delete("/api/saves/{save_id}", response_model=SuccessMessage)
def delete_save(
    save_id: int,
    repo: ClaimsRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response = repo.delete(save_id, user_id=account_data["id"])
    if response == {"success": False}:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return {"success": True}
