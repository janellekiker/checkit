from queries.claims import ClaimsRepository
from models import ClaimIn, UpdateClaim
from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator


client = TestClient(app)


class FakeClaimsRepository:
    def get_all(self, user_id: int):
        return []

    def delete(self, save_id: int, user_id: int):
        return {"success": True}

    def create(self, claim: ClaimIn, user_id: int):
        return {
            "id": 1337,
            "text": claim.text,
            "claimant": claim.claimant,
            "date": claim.date,
            "textualRating": claim.textualRating,
            "publisher": claim.publisher,
            "url": claim.url,
            "category_name": claim.category_name,
            "user_id": user_id,
        }

    def update(self, save_id: int, category_name: UpdateClaim, user_id: int):
        return {"success": True}


def fake_get_current_account_data():
    return {"id": "1111"}


def test_get_all():
    # Arrange
    app.dependency_overrides[ClaimsRepository] = FakeClaimsRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    # Act
    res = client.get("api/saves")
    data = res.json()

    # Assert
    assert res.status_code == 200
    assert data == []

    # Cleanup
    app.dependency_overrides = {}


def test_delete_save():
    # ARRANGE
    app.dependency_overrides[ClaimsRepository] = FakeClaimsRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # ACT
    res = client.delete("/api/saves/123")
    data = res.json()

    # ASSERT SOME TYPE OF BOOLEAN EXPRESSION
    # TRUE WE'RE GOOD, FALSE WE FAILED THE TEST
    assert res.status_code == 200
    assert data == {"success": True}

    # CLEANUP
    app.dependency_overrides = {}


def test_update_save():
    app.dependency_overrides[ClaimsRepository] = FakeClaimsRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    save_id = 123
    category_name = UpdateClaim(category_name="political")

    res = client.put(f"/api/saves/{save_id}", json=category_name.dict())
    data = res.json()

    assert res.status_code == 200
    assert data == {"success": True}

    app.dependency_overrides = {}


# liam
def test_create():
    app.dependency_overrides[ClaimsRepository] = FakeClaimsRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    claim_in = {
        "text": "test-title",
        "claimant": "test-claimant",
        "date": "2023-05-16T00:00:00Z",
        "textualRating": "False",
        "publisher": "test-publisher",
        "url": "http://testurl.com/test",
        "category_name": "test-category",
    }
    res = client.post("/api/saves", json=claim_in)
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "id": 1337,
        "text": "test-title",
        "claimant": "test-claimant",
        "date": "2023-05-16T00:00:00Z",
        "textualRating": "False",
        "publisher": "test-publisher",
        "url": "http://testurl.com/test",
        "category_name": "test-category",
        "user_id": 1111,
    }

    app.dependency_overrides = {}
