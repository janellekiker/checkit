from fastapi.testclient import TestClient
from main import app
import requests
from queries.claims import ThirdPartyClaimRespository
import os
import warnings

SECRET_API_KEY = os.environ["SECRET_API_KEY"]

client = TestClient(app)


class FakeThirdPartyClaimRespository:
    def get_all_claims(self, query: str):
        return {
            "claims": [
                {
                    "text": """
                    Barack Obama a déclaré qu'une
                    censure totale du gouvernement est nécessaire
                    pour éradiquer les médias indépendants.
                    """,
                    "claimDate": "2023-05-24T00:00:00Z",
                    "claimReview": [
                        {
                            "publisher": {
                                "name": "dpa-factchecking",
                                "site": "dpa-factchecking.com",
                            },
                            "url": (
                                "https://dpa-factchecking.com"
                                "/belgium/230525-99-823974/"
                            ),
                            "title": """
                            Interview déformée - Des propos d'Obama
                            au sujet des médias qui ont été inventés
                            """,
                            "reviewDate": "2023-05-26T00:00:00Z",
                            "textualRating": """
                            Barack Obama a en effet commenté
                            l'état du paysage médiatique aux États-Unis
                            auprès de la télévision américaine CBS.
                            Il n’a toutefois pas appelé à une censure
                            des médias indépendants. Le texte relayé sur
                            les réseaux sociaux provient en réalité d'un
                            site anglophone connu pour fabriquer des citations.
                            """,
                            "languageCode": "fr",
                        }
                    ],
                },
            ],
            "nextPageToken": "CAo",
        }


def test_get_third_party_claims():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    app.dependency_overrides[
        ThirdPartyClaimRespository
    ] = FakeThirdPartyClaimRespository
    res = requests.get(
        (
            "https://factchecktools.googleapis.com/"
            f"v1alpha1/claims:search?key={SECRET_API_KEY}"
            "&languageCode=en-US&query=Obama"
        )
    )
    data = res.json()

    assert res.status_code == 200
    assert "Obama" in data["claims"][1]["text"]
