steps = [
    [
        # Create the table
        """
        CREATE TABLE categories (
            id SERIAL NOT NULL UNIQUE,
            category_name VARCHAR(1000) NOT NULL,
            user_id INTEGER NOT NULL
                    REFERENCES accounts("id") ON DELETE CASCADE,
            CONSTRAINT pk_categories PRIMARY KEY (user_id, category_name)
        );
        """,
        # Drop the table
        """
        DROP TABLE categories;
        """,
    ]
]
