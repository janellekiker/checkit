Fri. June 2
Added logo to app, and also started deleting comments, unused variables, and refactoring.

Thurs. June 1
Fixed small details, like lowercase searches input instead of just uppercase. Also worked on making the site dark mode and light mode option, and added loading logo while user waits for claim to fetch.

Wed. May 31
Spend most of the day error handling, fixing no user error message when wrong password is written on the login form to show message when wrong error is written.

Tues. May 30
Worked on 404 page with Janelle. Gabby worked on making pages refresh when changed deleted claim and changed category. Also worked on protecting for empty data so we can still save. Started working as a group on handling errors.

Fri. May 26
Started working on unit test with Gabby, Liam worked on making a category pop up when choosing from dropdown or making category and delete button to delete from claims. We assigned a unit test per person from routers over Memorial Day weekend.

Thurs. May 25
Janelle took the lead on writing the code and continuing to work on how to make the page save the category that we had just written in the input bar, we noticed that by using input and save it as a props would help make the button work the way we had thought of when designing it and it worked. Saving the claim while also saving it in a category of their choice or new category (if they created it). Mo worked on saved claims.


Wed. May 24
Today I wrote the code for logout button, Gabby and Janelle helped with that. After that, Gabby took the lead on working on saving a claim with the category dropdown being updated and save the claim after you hit save. We really hit a wall here trying to make 1 button do two jobs, trying to figure out how to either make it re-render or change the whole code. We researched and asked SEIRS and instructors if it could be possible but it was too close to finishing the day so we stopped and left it for the following day.


Tue. May 23
Challenging day working on frontend, but really endured until we found what was wrong with our login, and signup page, we had “localhost:undefined” until we were able to see that we needed to pass the baseUrl into Authprovider in our app.js file to be able to actually process one token. Many tries but glad we stuck to this one. Also had time to work on the categories dropdown, and works great.

Mon. May 22
Today we started frontend. We were almost done completing writing the search page/homepage with redux but we hit a big wall, and went back to normal react. We had spent all day doing that so I think we did our best.

Thurs. May 18
Today we worked together as a team to debug code and fix some naming variable conventions, keep everything consistant, organized and clean. Mo finished the backend strong by doing categories' update/delete, made sure we named everything along restful "plural" routers. Tested out and worked perfectly!


Wed. May 17
We split up and worked on different parts of back end. I drove while getting guidance from Liam and Mo, we worked on put and delete routers and queries for claims tested out, several bugs, but now it works perfect! Janelle, and Gabby worked on post and get for categories. Also learned that you can go down on migrations (tables) and go up if needed to reverse a property or add one. Specially for the bug we had.

Tue. May 16
We worked on setting JWTdown for FastAPI with our PostgresSQL database, started with authenticator.py, log in and log out, and letting people sign up for new accounts. We kept/tried debugging the unauthorized message in fastapi response when creating the username and password, but we will keep working at it tomorrow morning. It comes down to we didn't need [0] on our fetchone.

Mon. May 15
Today we set up directories; queries and claims. Also worked on creating tables for the endpoints that we will be using, claim and categories CRUD for fastapi. We worked together on writing routes and also took awhile to debug Mapping import error from collections, we had to go down on our Requests library to 2.24.0.
