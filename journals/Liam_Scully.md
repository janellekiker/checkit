05/15/2023

Worked as a team to create our migrations, routers, and queries, as well as receive data from our 3rd party API through our backend. Current endpoints allow us to receive data (in our case, “claims”) from the third party API, save specific claims to the database, and get a list of saved claims. We can also create categories and get a list of categories. One of our blockers was the wrong version of requests being used, but we fixed it. Another blocker was our inability to import our API key from environment variables, but we fixed that as well.

05/16/2023

Coded most of backend auth as a group. Worked on this all day, nearly finishing by the end of the day. Still some bugs to work out.

05/17/2023

Gabby fixed the remaining bugs in our backend auth. Now fully working. Judith, Mo and myself worked on "PUT" and "DELETE" routers and queries for saved claims. Judith did an awesome job in the driver's seat.

05/18/2023

With Mo in the driver's seat, we finished our "PUT" and "DELETE" methods for categories. We also cleaned up some code and made sure all of our routes were RESTful.

05/22/2023

Kicked off frontend development. Gabby did a great job getting our home page and search bar to function. We decided against using Redux as we could not get it working for our use case.

05/23/2023

In my absence, the group did a fantastic job working on frontend auth. Sign up and login finished.

05/24/2023

Judith, Gabby and Janelle got the logout button functioning. Gabby got the category dropdown and save button working for claims. Logged in users now can save claims and select a pre-existing category from the dropdown. We wanted to make it so they can create a new category and save it at the same time as the claim, but we are having some issues with this method. Category does not post quick enough before the claims post. More work to be done.

05/25/2023

With Janelle in the driver's seat, we solved yesterday's issue with saving a claim and category together. Users can now create a new category at the same time as saving a claim. The new category will be tied to the claim as the claim is saved. We also added the ability to filter saved claims by category on the saved claims page.

05/26/2023

I was in the driver's seat today. I did some research on React-Bootstrap and learned how to make modals in React. Added a "change category" and "delete category" button for each saved claim. These buttons cause modals to pop up. The "change category" modal uses the same logic as our search page, where a user can choose a pre-exisiting category or make a new one and update the claim with their new category. We also chose which unit tests each group member will write. I chose the to write the unit test for the "POST" route on saved claims.

05/30/2023

Fixed our "update" method in the claims repository so that it returns {"success": False} when a user tries to update a non-existent claim. I did this by checking for rowcount when the update method is called. If rowcount is greater than zero, it will return {"success": True}. When rowcount is greater than zero, that means something was actually updated, which is why I chose this solution. I also finished writing my unit test today.

05/31/2023

Helped Gabby with very strange warnings on her unit test. Her unit test was finished and functioning properly, but she kept receiving deprecation warnings. We thoroughly researched these deprecation warnings as a group and we came to the conclusion that the best option was to include logic in the unit test that will ignore the deprecation warnings. Additionally, we worked for a very long time on our Login page. When login is succesful with correct, existing credentials, we wanted to redirect to our homepage. Otherwise, we wanted to display a message saying "Incorrect username or password". We banged our heads on this one for a long time and could not seem to figure it out. Although I departed a bit early, Gabby and the rest of the group were able to reach a solution with the help of some awesome SEIRs.

6/1/2023

We spent all day customizing our CSS and getting our dark mode/light mode toggler to work. That completes the last feature promised on our wireframe.\

6/2/2023

I changed our dark mode and light mode button to have background images for each state, Gabby added a spinning logo to our search. Janelle and Judith worked on cleaning up our code.
