# Wireframes

## HomePage
This will be the first page when a user visits our website. This is also the search page, if a user is already logged in they will see their two most recently saved claims.
![Not Logged In Homepage](./images/homepageNoLogin.png)
![Logged In Homepage](./images/homepageLoggedIn.png)

## Claim Results
The lower half of the page will change to display the results from the user's search. If the user is logged in they will have the ability to save a claim, if they aren't they will have a button asking them to create an account.
![Not Logged In Claim Results](./images/claimResultsNoLogin.png)
![Logged In Claim Results](./images/claimResultsLoggedIn.png)

## My Saved Page
After a logged in user has saved claims they can see all of them on this page. If they want to filter by a certain category they just need to select which category they want from the dropdown.
![My Saved Page](./images/mySaved.png)

## Signup Page
![Signup Page](./images/signup.png)

## Login Page
![Login Page](./images/login.png)
