import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function DeleteCategoryModal(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const { token } = useAuthContext();

    const handleSubmit = async (event) => {
        event.preventDefault();
        const deleteCatUrl = `${process.env.REACT_APP_API_HOST}/api/categories/${props.category_name}`;
        const fetchConfig = {
            method: "delete",
            credentials: "include",
            headers: {
                Authorization: `${token}`,
            },
        };
        const response = await fetch(deleteCatUrl, fetchConfig);
        if (response.ok) {
            setShow(false);
            window.location.reload();
        }
    };

    return (
        <>
            <Button
                variant="btn btn-outline-secondary"
                id="delete-cat"
                onClick={handleShow}
            >
                Delete category
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title id="delete-pop" className="popup-title">
                        Are you sure you want to delete this category?
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body id="delete-pop" className="popup-title">
                    Deleting a category will delete all saved claims associated with it.
                    If you would like to preserve these claims, change their categories
                    first.
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="btn btn-outline-secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="danger" onClick={handleSubmit}>
                        Delete category
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default DeleteCategoryModal;
