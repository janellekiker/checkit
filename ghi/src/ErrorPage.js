import React from 'react'

const ErrorPage = () => {
    return (
        <div className="container">
            <div>
                <h1 style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: '5px'
                }}
                >
                    404 Error</h1>
                <h2 style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: '5px'
                }}
                >
                    The Truth Could Not Be Found</h2>
            </div>
        </div>
    )
}

export default ErrorPage;
