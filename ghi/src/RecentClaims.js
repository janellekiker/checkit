import React, { useState, useEffect } from "react";
import "./App.css";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function RecentClaims(props) {
    const { token } = useAuthContext();
    const options = { month: "long", day: "numeric", year: "numeric" };
    const [saves, setSaves] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/saves`;

            const response = await fetch(url, {
                credentials: "include",
                headers: {
                    Authorization: `${token}`,
                },
            });

            if (response.ok) {
                const data = await response.json();
                if (data.length >= 2) {
                    setSaves([data[data.length - 1], data[data.length - 2]]);
                }
            }
        };

        fetchData();
    }, [token]);

    if (saves.length) {
        return (

            <div className={`container ${props.darkCont}`}>
                <ul>
                    <h2>Your recently saved claims:</h2>
                    {saves.map((item) => {
                        return (
                            <React.Fragment key={item.id}>
                                <li className={props.smollCont} value={item.text} >
                                    <div>
                                        <b>The claim: </b>
                                        {item.text}{" "}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The date:</b>{" "}
                                        {item.date !== "Not available"
                                            ? new Date(item.date).toLocaleDateString("en-US", options)
                                            : "Not available"}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The platform:</b> {item.claimant}
                                    </div>
                                    <br />
                                    <div>
                                        <b>True or False: </b>
                                        {item.textualRating}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The reviewer: </b>
                                        {item.publisher}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The website: </b>
                                        <a href={item.url}>{item.url}</a>
                                    </div>
                                </li>
                            </React.Fragment>
                        );
                    })}
                </ul>
            </div>
        );
    }
}
export default RecentClaims;
