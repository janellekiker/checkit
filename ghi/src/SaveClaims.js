import React, { useState, useEffect } from "react";
import "./App.css";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import CategoryModal from "./CategoryModal";
import DeleteCategoryModal from "./DeleteCategoryModal";
import DeleteClaim from "./DeleteClaim";

function SaveClaims(props) {
    const options = { month: "long", day: "numeric", year: "numeric" };
    const [saves, setSaves] = useState([]);
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState("");
    const { token } = useAuthContext();

    const handleCategoryChange = (event) => {
        let value = event.target.value;
        setSelectedCategory(value);
    };

    useEffect(() => {
        const fetchCategoryData = async () => {
            if (token) {
                const categoryUrl = `${process.env.REACT_APP_API_HOST}/api/categories/`;
                const categoryResponse = await fetch(categoryUrl, {
                    credentials: "include",
                    headers: {
                        Authorization: `${token}`,
                    },
                });

                if (categoryResponse.ok) {
                    const categoryData = await categoryResponse.json();
                    setCategories(categoryData);
                }
            };
        }

        const fetchData = async () => {
            if (token) {
                const url = `${process.env.REACT_APP_API_HOST}/api/saves`;
                const response = await fetch(url, {
                    credentials: "include",
                    headers: {
                        Authorization: `${token}`,
                    },
                });

                if (response.ok) {
                    const data = await response.json();
                    if (data.length >= 1) {
                        setSaves(data);
                    }
                }
            };
        }

        if (token != null) {
            fetchCategoryData();
            fetchData();
        }
    }, [token]);

    const filteredData = saves.filter(
        (save) => !selectedCategory || save.category_name === selectedCategory
    );


    if (saves.length > 1) {
        return (
            <div className={`container ${props.darkCont}`}>
                <ul>
                    <h2>Your saved claims:</h2>
                    <select
                        value={selectedCategory}
                        onChange={handleCategoryChange}
                        name="category"
                        id="category"
                        className="form-select"
                    >
                        <option value="">Choose a category</option>
                        {categories.map((category) => {
                            return (
                                <option key={category.id} value={category.category_name}>
                                    {category.category_name}
                                </option>
                            );
                        })}
                    </select>
                    {filteredData.map((item) => {
                        return (
                            <React.Fragment key={item.id}>
                                <li
                                    className={props.smollCont}
                                    value={item.text}
                                    style={{ position: "relative" }}
                                >
                                    <div>
                                        <b>The claim: </b>
                                        {item.text}{" "}
                                        <div>
                                            <DeleteClaim save_id={item.id} />
                                        </div>
                                    </div>
                                    <br />
                                    <div>
                                        <b>The date:</b>{" "}
                                        {item.date !== "Not available"
                                            ? new Date(item.date).toLocaleDateString("en-US", options)
                                            : "Not available"}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The platform:</b> {item.claimant}
                                    </div>
                                    <br />
                                    <div>
                                        <b>True or False: </b>
                                        {item.textualRating}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The reviewer: </b>
                                        {item.publisher}
                                    </div>
                                    <br />
                                    <div>
                                        <b>The website: </b>
                                        <a href={item.url}>{item.url}</a>
                                    </div>
                                    <br />
                                    <div>
                                        <b>Category: </b>
                                        {item.category_name}
                                    </div>
                                    <br />
                                    <CategoryModal save_id={item.id} />{" "}
                                    <DeleteCategoryModal category_name={item.category_name} />
                                </li>
                            </React.Fragment>
                        );
                    })}

                </ul>
            </div>
        );
    }
}
export default SaveClaims;
