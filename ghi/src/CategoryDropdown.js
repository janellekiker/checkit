import React, { useState, useEffect } from "react";
import "./App.css";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import SaveButton from "./SaveButton";

function CategoryDropdown(props) {
    const { token } = useAuthContext();
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState("");
    const [NewCat, setNewCat] = useState("");

    const handleCategoryChange = (event) => {
        const value = event.target.value;
        setCategory(value);
    };
    const handleNewCatChange = (event) => {
        const value = event.target.value;
        setNewCat(value);
    };

    useEffect(() => {
        const fetchData = async () => {
            const url = "http://localhost:8000/api/categories";

            const response = await fetch(url, {
                credentials: "include",
                headers: { Authorization: `${token}` },
            });

            if (response.ok) {
                const data = await response.json();
                setCategories(data);
            }
        };

        fetchData();
    }, [token]);

    return (
        <>
            <form>
                <p>Want to save this claim?</p>
                <div className="dropdown">
                    <select
                        className="form-select"
                        onChange={handleCategoryChange}
                        name="category"
                        required
                        type="text"
                        value={category}
                    >
                        <option value="">Choose a Category</option>
                        {categories.map((c) => {
                            return (
                                <option key={c.id} value={c.category_name}>
                                    {c.category_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div>
                    <p> Or, Make a new Category</p>
                    <input
                        onChange={handleNewCatChange}
                        className={props.darkInput}
                        type="text"
                        value={NewCat}
                    />
                </div>
            </form>
            <SaveButton item={props.item} category={category} NewCat={NewCat} />
        </>
    );
}

export default CategoryDropdown;
