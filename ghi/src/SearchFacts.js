import React, { useState } from "react";
import "./App.css";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import Claims from "./Claims";
import RecentClaims from "./RecentClaims";
import Loading from "./Loading";
import BlinkingHeader from "./BlinkingHeader";


function SearchFacts(props) {
	const { token } = useAuthContext();
	const [claim, setClaim] = useState("");
	const [claims, setClaims] = useState([]);
	const [searchDone, setSearchDone] = useState(false);
	const [hidden, setHidden] = useState("hidden")


	const handleClaimChange = (event) => {
		const value = event.target.value;
		setClaim(value);
		setHidden("spin")
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const claimUrl = `${process.env.REACT_APP_API_HOST}/api/claims?query=${claim}`;
		const fetchConfig = {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
			},

		};

		const response = await fetch(claimUrl, fetchConfig);
		if (response.ok) {
			const data = await response.json();
			setClaims(data.claim.claims);
			setSearchDone(true);
			setHidden("hidden")
		}
	};

	return (
		<>
			<form onSubmit={handleSubmit}>
				<div className="claim">
					<div className="main">
						<div className="offset-3 col-6">
							<BlinkingHeader darkHeader={props.darkHeader} />
							<div className="input-group mb-3">
								<div className="input-group-prepend">
									<input
										onChange={handleClaimChange}
										value={claim}
										id="outlined-basic"
										type="text"
										className={props.darkInput}
										placeholder=""
										aria-label="search"
										style={{ width: '450px' }}
										aria-describedby="basic-addon1"
									/>
								</div>
								<button
									onClick={handleSubmit}
									className="btn btn-outline-secondary"
									type="button"
								>
									{" "}
									Check it.{" "}
								</button>
							</div>
							<Loading hidden={hidden} />
						</div>
					</div>
				</div>
			</form>
			{token && !searchDone ? <RecentClaims darkHeader={props.darkHeader} darkCont={props.darkCont} smollCont={props.smollCont} /> : null}
			{claims ? <Claims claims={claims} claim={claim} darkCont={props.darkCont} smollCont={props.smollCont} /> : <h2 style={{
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
				marginTop: '5px'
			}}>Sorry, no claims were found. Try searching something else.</h2>}
		</>
	);
}
export default SearchFacts;
