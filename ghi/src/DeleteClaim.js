import Button from "react-bootstrap/Button";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function DeleteClaim(props) {
    const { token } = useAuthContext();

    const handleClick = async (event) => {
        event.preventDefault();
        const deleteSaveUrl = `${process.env.REACT_APP_API_HOST}/api/saves/${props.save_id}`;
        const fetchConfig = {
            method: "delete",
            credentials: "include",
            headers: {
                Authorization: `${token}`,
            },
        };
        const response = await fetch(deleteSaveUrl, fetchConfig);
        if (response.ok) {
            window.location.reload();
        }
    };

    return (
        <>
            <Button
                variant="btn btn-outline-secondary"
                className="delete-claim"
                onClick={handleClick}
                style={{ position: "absolute", bottom: "15px", right: "40px" }}
            >
                Delete claim
            </Button>
        </>
    );
}

export default DeleteClaim;
